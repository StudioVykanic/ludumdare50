/* Based on https://answers.unity.com/questions/1414634/can-i-add-an-enum-value-in-the-inspector.html
 * Edits and additions by Studio Vykanic LLC
 * 
 * Used to write strings to an enum and have unity load the new file
 */

using System.Collections.Generic;
using System.IO;
using UnityEditor;
using static CustomTypes;

public class CustomEnum : Editor
{
    const string extension = ".cs";

    public static void WriteMultipleToEnum<K,V>(string path, string name, List<CustomKeyValueList<K,V>> data)
    {
        using (StreamWriter file = File.CreateText(path + name + extension))
        {
            file.WriteLine("public class " + name + " \n{");
            foreach (var item in data)
            {

                file.WriteLine("public enum " + item._Key + " \n{");
                int i = 0;
                foreach (var line in item._Value)
                {
                    string lineRep = line.ToString().Replace(" ", string.Empty);
                    if (!string.IsNullOrEmpty(lineRep))
                    {
                        file.WriteLine(string.Format("\t{0} = {1},",
                            lineRep, i));
                        i++;
                    }
                }
                file.WriteLine("\n}");
            }
            file.WriteLine("\n}");
        }

        AssetDatabase.ImportAsset(path + name + extension);
    }
}
