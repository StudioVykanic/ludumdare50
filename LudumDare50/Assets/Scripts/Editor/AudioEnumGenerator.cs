/* Based on https://answers.unity.com/questions/1414634/can-i-add-an-enum-value-in-the-inspector.html
 * Edits and additions by Studio Vykanic LLC
 * 
 * Adds a button to AudioList scriptable object to generate enums for the audio list
 */

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AudioListSO))]
public class AudioEnumGenerator : Editor
{
    AudioListSO audioList;
    string filePath = "Assets/Scripts/Audio/";
    string fileName = "AudioTypes";

    private void OnEnable()
    {
        audioList = (AudioListSO)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        filePath = EditorGUILayout.TextField("Path", filePath);
        fileName = EditorGUILayout.TextField("Name", fileName);
        if (GUILayout.Button("Generate Enums"))
        {
            CustomEnum.WriteMultipleToEnum<string,string>(filePath, fileName, audioList.AudioNames);
        }
    }
}
