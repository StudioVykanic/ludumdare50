﻿/* Copyright (C) Studio Vykanic LLC
 * 
 * Defines an audio clips pitch, volume, and source audio clip
 */

using UnityEngine;

[CreateAssetMenu(fileName = "AudioClipSO", menuName = "ScriptableObjects/Audio/AudioClipSO")]
public class AudioClipSO : ScriptableObject
{
    public AudioClip Clip;
    public float PitchLow = 1f;
    public float PitchHigh = 1f;
    public float Pitch
    {
        get { return Random.Range(PitchLow, PitchHigh); }
        private set{}
    }
    public float Volume = 1f;
}
