/* Copyright (C) Studio Vykanic LLC
 * 
 * Used to setup a list of audio clips in the Unity Editor 
 */

using System.Collections.Generic;
using UnityEngine;
using static CustomTypes;

[CreateAssetMenu(fileName = "AudioListSO", menuName = "ScriptableObjects/Audio/AudioListSO")]
public class AudioListSO : ScriptableObject
{
    public List<CustomKeyValueList<string, string>> AudioNames;
    public List<CustomKeyValue<AudioTypes.Effects, AudioClipSO>> EffectClips;
    public List<CustomKeyValue<AudioTypes.Music, AudioClipSO>> MusicClips;
    public AudioClipSO[] EffectClipsArray;
    public AudioClipSO[] MusicClipsArray;
    
    public void GenerateArrays()
    {
        EffectClipsArray = new AudioClipSO[EffectClips.Count];
        foreach (var item in EffectClips)
        {
            EffectClipsArray[(int)item._Key] = item._Value;
        }

        MusicClipsArray = new AudioClipSO[MusicClips.Count];
        foreach (var item in MusicClips)
        {
            MusicClipsArray[(int)item._Key] = item._Value;
        }
    }
}
