﻿/* Copyright (C) Studio Vykanic LLC
 * 
 * Manages starting and stopping of audio clips. Has Fade In/Out Methods.
 * Audio types come from enums that are generated in Unity Editor and this class gets an array of what clips those enums correspond to.
 * 
 * Still a work in progress... TODO.. continue to clean up the code, probably integrate Addressables, update to use source pool.
 */

using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public AudioListSO AudioList; //the scriptable object that contains all audio clips

    public AudioSource EffectsAudioSource;
    public AudioSource EffectsAudioSource2;
    public AudioSource MusicAudioSource;
    public AudioSource AmbientAudioSource;

    public float FadeDuration = 1;

    public void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        AudioList.GenerateArrays();
    }

    //Starts playing a music clip on the music audio source
    public void PlayMusic(AudioTypes.Music musicType)
    {
        MusicAudioSource.clip = AudioList.MusicClipsArray[(int)musicType].Clip;
        MusicAudioSource.Play();
    }

    //Stops playing the music audio source
    public void StopMusic()
    {
        MusicAudioSource.Stop();
    }

    //Starts playing an ambient clip on the ambient audio source
    public void PlayAmbient(AudioTypes.Music ambientType)
    {
        AmbientAudioSource.clip = AudioList.MusicClipsArray[(int)ambientType].Clip;
        AmbientAudioSource.Play();
    }

    //Stops playing the ambient audio source
    public void StopAmbient()
    {
        AmbientAudioSource.Stop();
    }

    //Plays a sound effect
    public void PlayOneShotEffect(AudioTypes.Effects effectType)
    {
        EffectsAudioSource.pitch = AudioList.EffectClipsArray[(int)effectType].Pitch;
        EffectsAudioSource.volume = AudioList.EffectClipsArray[(int)effectType].Volume;
        Debug.Log(AudioList.EffectClipsArray[(int)effectType].Volume);
        EffectsAudioSource.PlayOneShot(AudioList.EffectClipsArray[(int)effectType].Clip);
    }

    public void PlayOneShotEffect(AudioTypes.Effects effectType, AudioSource source)
    {
        source.pitch = AudioList.EffectClipsArray[(int)effectType].Pitch;
        source.volume = AudioList.EffectClipsArray[(int)effectType].Volume;
        Debug.Log(AudioList.EffectClipsArray[(int)effectType].Volume);
        source.PlayOneShot(AudioList.EffectClipsArray[(int)effectType].Clip);
    }

    //Can fade in an audio source to a clips max volume
    public void FadeInAudio(AudioSource source, AudioTypes.Music type)
    {
        StartCoroutine(FadeIn(source,type));
    }

    //Can fade out an audio source
    public void FadeOutAudio(AudioSource source, bool shouldStop)
    {
        StartCoroutine(FadeOut(source, shouldStop));
    }

    //Can fade in an audio source to a clips max volume
    private IEnumerator FadeIn(AudioSource source, AudioTypes.Music type)
    {  
        float currentTime = 0;
        float start = source.volume;
        float maxVolume = AudioList.MusicClipsArray[(int)type].Volume;

        while (currentTime < FadeDuration)
        {
            currentTime += Time.deltaTime;
            source.volume = Mathf.Lerp(start, maxVolume, currentTime / FadeDuration);
            yield return null;
        }
        
        yield break;
    }

    //Can fade out an audio source
    private IEnumerator FadeOut(AudioSource source, bool shouldStop)
    {
        float currentTime = 0;
        float start = source.volume;

        while (currentTime < FadeDuration)
        {
            currentTime += Time.deltaTime;
            source.volume = Mathf.Lerp(start, 0, currentTime / FadeDuration);
            yield return null;
        }

        if (shouldStop)
        {
            source.Stop();
        }

        yield break;
    }
}
