/* Copyright (C) Studio Vykanic LLC
 * 
 * Some custom key/value types that serialize in the Unity editor
 */

using System.Collections.Generic;

public static class CustomTypes
{
    [System.Serializable]
    public class CustomKeyValue<K, V>
    {
        public K _Key;
        public V _Value;
    }

    [System.Serializable]
    public class CustomKeyValueList<K, V>
    {
        public K _Key;
        public List<V> _Value;
    }
}
