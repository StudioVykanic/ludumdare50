using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.CompareTag("Draggable"))
        {
            collision.gameObject.tag = "Steady";
            collision.gameObject.GetComponent<RigidbodyFollow>().isDragging = false;
            AudioManager.instance.PlayOneShotEffect(AudioTypes.Effects.Dropoff, AudioManager.instance.EffectsAudioSource2);
        }

        if (collision.transform.CompareTag("Player"))
        {
            if (!MovementForce.Dead)
            {
                AudioManager.instance.PlayOneShotEffect(AudioTypes.Effects.Startup);
                AudioManager.instance.StopAmbient();
                GameManager.instance.DestroyLaser();
                GameManager.instance.ShowWinText();
            }
        }

    }
}
