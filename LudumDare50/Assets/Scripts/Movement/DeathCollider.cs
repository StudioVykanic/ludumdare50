using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathCollider : MonoBehaviour
{
    public static bool Collided = false;
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            if (!Collided)
            {
                //explode player
                collision.gameObject.GetComponent<MovementForce>().OnDeath();
                Collided = true;
            }
        }
    }
}
