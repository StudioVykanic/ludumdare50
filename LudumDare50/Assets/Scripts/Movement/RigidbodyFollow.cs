using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RigidbodyFollow : MonoBehaviour
{
    public bool isDragging = false;
    private Rigidbody _rigidbody;
    public float force = 20f;
    public float maxVelocity = 40f;
    public float distance = 24.3f;

    public float min = -2.5f; //eyeballed hack due to my camera angle and perspective
    public float maxDist = 28.3f; //eyeballed hack due to my camera angle and perspective
    public bool  once = true;
    public bool once2 = true;

    void Start()
    {
        _rigidbody = this.GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
       /* if(collision.gameObject.CompareTag("Steady") && once)
        {
            AudioManager.instance.PlayOneShotEffect(AudioTypes.Effects.BlockHit);
            once = false;
        }*/
    }

    private void FixedUpdate()
    {
        if (isDragging)
        {
            distance = maxDist - ((6f / 16f) * (transform.position.y - min)); //eyeballed hack due to my camera angle and perspective
            Vector3 mousePos = Mouse.current.position.ReadValue();
            Vector3 mousePos2 = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, distance));

            mousePos2.z = 0;
            Vector3 direction = mousePos2 - transform.position;

            _rigidbody.velocity = direction.normalized * (Mathf.Min(maxVelocity, force * direction.magnitude));

        }
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}
