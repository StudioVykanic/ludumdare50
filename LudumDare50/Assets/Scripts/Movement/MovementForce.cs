using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MovementForce : MonoBehaviour
{
    public Vector2 MoveAmount;
    public float Speed = 10f;
    public float AccelRate = 1f;
    public float DeccelRate = 1f;
    public Vector3 FinalForce;
    private Vector3 _input;
    private Rigidbody _rigidbody;
    public bool shouldJump = false;
    public float jumpForce = 10f;

    public float sphereRadius = 1f;
    public float sphereDistance = .25f;

    public static bool Dead = false;
    public GameObject DeathExplosion;
    public GameObject BotObject;

    void Start()
    {
        _rigidbody = this.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (!Dead)
        {
            _input.x = MoveAmount.x;
            Vector3 targetSpeed = _input * Speed;
            Vector3 speedDiff = targetSpeed - _rigidbody.velocity;
            float rateX = !Mathf.Approximately(targetSpeed.x, 0) ? AccelRate : DeccelRate;
            FinalForce.x = Mathf.Abs(speedDiff.x) * rateX * Mathf.Sign(speedDiff.x);
            FinalForce.z = 0;
            _rigidbody.AddForce(FinalForce);
        }
    }

    public void OnMove(InputValue value)
    {
        MoveAmount = value.Get<Vector2>();
    }

    public void OnJump(InputValue value)
    {
        if (!Dead)
        {
            //todo: check if grounded
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, sphereRadius, Vector3.down, out hit, sphereDistance))
            {
                Debug.Log(hit.distance);

                Vector3 force = new Vector3(0, jumpForce, 0);
                _rigidbody.AddForce(force, ForceMode.Impulse);
                AudioManager.instance.PlayOneShotEffect(AudioTypes.Effects.BlockHit);
            }

        }
    }

    public void OnDeath()
    {
        Dead = true;
        _rigidbody.velocity = new Vector3(0, 0, 0);
        Instantiate(DeathExplosion, this.transform);
        BotObject.SetActive(false);
        AudioManager.instance.PlayOneShotEffect(AudioTypes.Effects.Explosion);
    }

    //Shouldn't be here but it is for now!
    public void OnRestart(InputValue value)
    {
        GameManager.instance.Restart();
    }
}
