using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DragObject : MonoBehaviour
{
    public bool dragging = false;
    private RigidbodyFollow _draggedRF;
    private Rigidbody _draggedR;

    public void OnDrag(InputValue context)
    {

        if (context.isPressed)
        {


            RaycastHit hit;
            Vector3 coor = Mouse.current.position.ReadValue();
            if (Physics.Raycast(Camera.main.ScreenPointToRay(coor), out hit))
            {
                if (hit.transform.CompareTag("Draggable"))
                {
                    Debug.Log(hit.distance);
                    dragging = true;
                    Debug.Log("hit block");
                    _draggedRF = hit.transform.gameObject.GetComponent<RigidbodyFollow>();
                    _draggedR = hit.transform.gameObject.GetComponent<Rigidbody>();
                    _draggedRF.isDragging = true;
                    _draggedR.collisionDetectionMode = CollisionDetectionMode.Continuous;
                    AudioManager.instance.PlayOneShotEffect(AudioTypes.Effects.Pickup, AudioManager.instance.EffectsAudioSource2);

                }
            }
        }
        if (!context.isPressed)
        {
            if (dragging)
            {
                Debug.Log("mouse let go");
                _draggedRF.isDragging = false;
                _draggedR.collisionDetectionMode = CollisionDetectionMode.Discrete;
                dragging = false;
            }
        }
    }
}
