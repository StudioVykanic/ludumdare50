using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetAnim : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Animation anim = GetComponent<Animation>();
        anim["Hop"].time = Random.Range(0.0f, anim["Hop"].length);
    }

}
