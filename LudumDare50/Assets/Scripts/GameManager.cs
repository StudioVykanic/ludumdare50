using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Transform SpawnLocation;
    public Transform BlockHolder;
    public List<GameObject> BlockTypes;
    public int BlockCountRows = 20;

    public bool Started = false;

    public Rigidbody WeaponRigidbody;
    public Transform WeaponTransform;
    public Vector3 WeaponStartPos;
    public Vector3 WeaponVelocity;

    public int LaserTime = 20;
    public int LaserWaitTime = 20;
    public TextMeshPro LaserTimeText;
    public GameObject DeathLaser;
    public GameObject DeathLaserCollider;
    public Transform DeathLaserColliderHolder;

    public GameObject StartScreen;
    public GameObject StoryScreen;

    public GameObject Player;
    public GameObject BotObject;
    private Vector3 _startPos;

    public GameObject WinText;


    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        HideWinText();
        AudioManager.instance.FadeInAudio(AudioManager.instance.MusicAudioSource, AudioTypes.Music.BackingTrack);
        _startPos = Player.transform.position;
        WeaponStartPos = WeaponTransform.position;
        MovementForce.Dead = false;
    }

    public void StartLaserTimer()
    {
        StartCoroutine(LaserTimer());
    }

    public void CloseStartScreen()
    {
        StartScreen.SetActive(false);
    }

    public void CloseStoryScreen()
    {
        StoryScreen.SetActive(false);
    }

    public void Restart()
    {
        HideWinText();
        AudioManager.instance.StopAmbient();
        WeaponRigidbody.velocity = new Vector3(0, 0, 0);
        StopAllCoroutines();
        DestroyExplosion();
        DeathCollider.Collided = false;
        BotObject.SetActive(true);
        Player.transform.position = _startPos;
        MovementForce.Dead = false;
        LaserTime = LaserWaitTime;
        LaserTimeText.text = LaserWaitTime.ToString();
        WeaponTransform.position = WeaponStartPos;
        StartLaserTimer();
        Started = true;
        DestroyBlocks();
        SpawnBlocks();
        DestroyLaser();
        AudioManager.instance.PlayOneShotEffect(AudioTypes.Effects.Startup);
    }

    // Update is called once per frame
    void Update()
    {
        if(Started)
        {
            LaserTimeText.text = LaserTime.ToString();
        }

        if (Keyboard.current.pKey.IsActuated())
        {
            SpawnBlocks();
        }
        if (Keyboard.current.lKey.IsActuated())
        {
            DestroyBlocks();
        }
    }

    private void FixedUpdate()
    {
        if(Started)
        {
            if (LaserTime == 0)
            {
                WeaponRigidbody.velocity = WeaponVelocity;
            }
        }
    }

    private IEnumerator LaserTimer()
    {
        for(int time = LaserWaitTime-1; time>=0; time--)
        {
            yield return new WaitForSeconds(1);
            LaserTime = time;
            AudioManager.instance.PlayOneShotEffect(AudioTypes.Effects.Timer);
        }
        SpawnLaser();
        yield break;
    }

    public void SpawnBlocks()
    {
        float y = SpawnLocation.position.y;
        for (int row = 0; row<BlockCountRows; row++)
        {
            float x = SpawnLocation.position.x;
            for (int z = 1; z <= 3; z++)
            {
                Vector3 pos = new Vector3();
                pos.y = y;
                pos.x = x;
                int i = Random.Range(0, BlockTypes.Count);
                if (i == 2)
                {
                    Instantiate(BlockTypes[i], pos, Quaternion.Euler(-90, 0, 0), BlockHolder); //hack for now need to fix blender rotation
                }
                else
                {
                    Instantiate(BlockTypes[i], pos, Quaternion.Euler(0, 0, 0), BlockHolder);
                }
                x += 2.25f;
            }

            y += 2f;
        }
    }

    public void SpawnLaser()
    {
        DeathLaser.SetActive(true);
        Instantiate(DeathLaserCollider, DeathLaserColliderHolder);
        AudioManager.instance.PlayAmbient(AudioTypes.Music.Wobble);
    }

    public void ShowWinText()
    {
        WinText.SetActive(true);
    }

    public void HideWinText()
    {
        WinText.SetActive(false);
    }

    public void DestroyLaser()
    {
        foreach (Transform laser in DeathLaserColliderHolder)
        {
            if (laser.CompareTag("DeathLaser"))
            {
                Destroy(laser.gameObject);
            }
        }
        DeathLaser.SetActive(false);
    }

    public void SpawnBlock()
    {
        Instantiate(BlockTypes[Random.Range(0, BlockTypes.Count)], SpawnLocation.position, Quaternion.Euler(-90,0,0), BlockHolder);
    }

    public void DestroyBlocks()
    {
        foreach(Transform block in BlockHolder)
        {
            Destroy(block.gameObject);
        }
    }

    public void DestroyExplosion()
    {
        foreach (Transform bot in Player.transform)
        {
            if (bot.CompareTag("ExplosionObject"))
            {
                Destroy(bot.gameObject);
            }
        }
    }
}
